using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using UnityMultiplayer.Shared;
using UnityMultiplayer.Shared.Messaging;

namespace UnityMultiplayer.GameServer
{
  /// <summary>
  ///   Handles the incoming player connections and the startup of match servers.
  /// </summary>
  public class GameServer : Server
  {
    protected int MatchIdCounter;

    public GameServer(string matchServerBinaryPath) => MatchServerBinary = matchServerBinaryPath;

    /// <summary>
    ///   Full path to the match server binary file.
    /// </summary>
    protected string MatchServerBinary { get; }

    /// <summary>
    ///   Reports the status of this game server to the master server.
    /// </summary>
    public GameServerClient GameServerClient { get; } = new GameServerClient();

    protected Dictionary<int, RemoteMatchServer> RemoteMatchServerDict { get; } =
      new Dictionary<int, RemoteMatchServer>();

    /// <summary>
    ///   Maps match server ids to <see cref="RemoteMatchServer" /> instances.
    /// </summary>
    public IReadOnlyDictionary<int, RemoteMatchServer> MatchServers => RemoteMatchServerDict;

    /// <summary>
    ///   Starts listening for incoming player connections and reports itself to the master server.
    /// </summary>
    /// <returns></returns>
    public override async Task StartAsync()
    {
      await base.StartAsync();
      await GameServerClient.ConnectToMasterAsync().ContinueWith(ConnectedToMasterCallback);
    }

    /// <summary>
    ///   Called after connecting to the master server. Announces itself to the master server if the connection was successful.
    /// </summary>
    /// <param name="connectTask"> The <see cref="Task" /> that represents the connection attempt to the master server. </param>
    protected void ConnectedToMasterCallback(Task connectTask)
    {
      if (connectTask.IsCompletedSuccessfully())
      {
        GameServerClient.MessageHandler.SendMessage(
          ServerMessageEncoder.AnnounceGameServerReady(GetOwnPublicAddress()));
      }
    }

    /// <summary>
    ///   Called when a player initiates a connection. Authenticates the player and starts the message loop.
    /// </summary>
    /// <param name="client">The <see cref="TcpClient" /> used to talk to the player.</param>
    protected override void BaseOnClientConnected(TcpClient client)
    {
      // TODO: Authenticate client with token
      AuthenticatedUser user = new AuthenticatedUser(client, new Random().Next());
      Log.Info($"Client {client.Client.RemoteEndPoint} authenticated as {user} using token authentication" +
        " with token {}");
      AuthenticatedUsersDict[user.Id] = user;
      user.MessageHandler.MessageReceived += UserOnMessageReceived;
      user.MessageHandler.StreamOperationException += UserOnStreamOperationException;
      user.MessageHandler.StartReadLoop();
      InvokeClientConnected(user);
    }

    /// <summary>
    ///   Called when there was an error reading/writing data from/to a player.
    ///   Disconnects the player on the assumption that they closed the connection.
    /// </summary>
    /// <param name="user">The <see cref="AuthenticatedUser" /> that triggered the error.</param>
    /// <param name="exception">The <see cref="Exception" /> thrown by the network API.</param>
    protected void UserOnStreamOperationException(AuthenticatedUser user, Exception exception)
    {
      DisconnectUser(user, false);
      Log.Info($"Disconnected {user} because an exception occured", exception);
    }

#pragma warning disable 4014
    /// <summary>
    ///   Handles received messages from players.
    /// </summary>
    /// <param name="user">The <see cref="AuthenticatedUser" /> that sent the message.</param>
    /// <param name="message">The <see cref="Message" /> sent by the player.</param>
    protected async void UserOnMessageReceived(AuthenticatedUser user, Message message)
    {
      Log.Debug($"Received {message} from {user}");
      Message response;
      switch (message.MessageType)
      {
        case MessageType.CreateMatch:
        {
          int matchId = GetNextMatchId();
          int port = GetNextAvailablePort();
          IPEndPoint endPoint = new IPEndPoint(GetOwnPublicAddress(), port);
          RemoteMatchServer remoteMatchServer = new RemoteMatchServer(user, matchId, endPoint,
            new ProcessStartInfo(MatchServerBinary,
              Utils.FormatArgument(Constants.PORT_ARG_NAME, port)));
          Log.Info($"Started {remoteMatchServer}");
          remoteMatchServer.ProcessHandle.Exited += RemoteMatchServerOnExited;
          RemoteMatchServerDict[matchId] = remoteMatchServer;
          response = MessageEncoder.CreateMatchResponse(remoteMatchServer);
          await Utils.WaitForPortListen(port);
          user.MessageHandler.SendMessage(response);
          DisconnectUser(user, true);
          return;
        }
        case MessageType.JoinMatch:
        {
          int matchId = MessageDecoder.JoinMatch(message);
          if (MatchServers.TryGetValue(matchId, out RemoteMatchServer server) && server.RemoteEndPoint != null)
          {
            response = MessageEncoder.JoinMatchResponseAccepted(server);
            user.MessageHandler.SendMessage(response);
            DisconnectUser(user, true);
            return;
          }

          response = MessageEncoder.JoinMatchResponseDeclined();
          break;
        }
        case MessageType.GetMatchList:
          IEnumerable<MatchInfo> matchInfos = MatchServers
            .Select(pair => pair.Value)
            .Where(server => server.RemoteEndPoint != null)
            .Select(server => (MatchInfo) server);
          response = MessageEncoder.PopulateMatchList(matchInfos);
          break;
        default:
          return;
      }

      user.MessageHandler.SendMessage(response);
      Log.Debug($"Sent response {response} to {user}");
    }
#pragma warning restore 4014

    /// <summary>
    ///   Called when a match server has exited, meaning the match is over.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RemoteMatchServerOnExited(object sender, EventArgs e)
    {
      RemoteMatchServer server = (RemoteMatchServer) sender;
      RemoteMatchServerDict.Remove(server.MatchId);
      int exitCode = server.ProcessHandle.ExitCode;
      string message = $"{server} exited with status code {server.ProcessHandle.ExitCode}";
      if (exitCode == 0)
        Log.Info(message);
      else
        Log.Warn(message);
    }

    protected int GetNextMatchId() => ++MatchIdCounter;
  }
}
