using System.Diagnostics;
using System.Net.Sockets;
using System.Threading.Tasks;
using UnityMultiplayer.Shared;
using UnityMultiplayer.Shared.Messaging;

namespace UnityMultiplayer.GameServer
{
  /// <summary>Talks to the master server on behalf of a <see cref="GameServer" />.</summary>
  public class GameServerClient : NetworkUser, IMessageHandler<GameServerClient, ServerMessage>
  {
    public GameServerClient() : this(new TcpClient()) { }

    protected GameServerClient(TcpClient tcpClient) : base(tcpClient) { }

    /// <summary>
    ///   Handles <see cref="Message" />s from the master server.
    /// </summary>
    public MessageHandler<GameServerClient, ServerMessage> MessageHandler { get; protected set; }

    public override void Dispose()
    {
      base.Dispose();
      MessageHandler.Dispose();
    }

    protected void InternalOnMessageReceived(GameServerClient self, ServerMessage message)
    {
      if (message.MessageType == ServerMessageType.MasterServerPing)
      {
        Log.Debug($"Received {message} from master");
        ServerMessage response = ServerMessageEncoder.GameServerPong();
        MessageHandler.SendMessage(response);
        Log.Debug($"Sent {response} to master");
      }
    }

    /// <summary>
    ///   Aborts any previous connection and starts connecting to the master server.
    /// </summary>
    /// <returns></returns>
    public async Task ConnectToMasterAsync()
    {
      Disconnect(0);
      const string ip = Constants.MASTER_SERVER_IP_ADDRESS;
      const int port = Constants.MASTER_SERVER_PORT;
      Log.Info($"Connecting to the master server {ip}:{port}");
      await TcpClient.ConnectAsync(ip, port).ContinueWith(ConnectCallback);
    }

    protected Task ConnectCallback(Task connectTask)
    {
      if (connectTask.IsCompletedSuccessfully())
      {
        Log.Info("Successfully connected to the master server");
        MessageHandler = new MessageHandler<GameServerClient, ServerMessage>(this, TcpClient.GetStream(),
          ServerMessageEncoder.ToByteArray, ServerMessageDecoder.FromByteArray);
        MessageHandler.MessageReceived += InternalOnMessageReceived;
        MessageHandler.StartReadLoop();
      }
      else if (connectTask.IsFaulted)
      {
        Debug.Assert(connectTask.Exception != null);
        Log.Warn("Failed to connect to the master server", connectTask.Exception);
      }
      else if (connectTask.IsCanceled)
        Log.Info("Connection to the master server was cancelled");

      return connectTask;
    }

    public override string ToString() =>
      $"<{nameof(GameServerClient)}" +
      $":{(TcpClient.Connected ? "Connected" : "Disconnected")}" +
      $":<RemoteEndPoint:{TcpClient.Client.RemoteEndPoint}>>";
  }
}
