using System.Diagnostics;
using System.Net;
using UnityMultiplayer.Shared;

namespace UnityMultiplayer.GameServer
{
  /// <summary>
  ///   Represents a match server.
  /// </summary>
  public class RemoteMatchServer : NetworkInfrastructureEntity
  {
    public RemoteMatchServer(AuthenticatedUser host, int matchId, IPEndPoint remoteEndPoint,
      ProcessStartInfo processStartInfo)
    {
      Host = host;
      MatchId = matchId;
      RemoteEndPoint = remoteEndPoint;
      ProcessHandle = Process.Start(processStartInfo);
    }

    public AuthenticatedUser Host { get; }

    public int MatchId { get; }

    public Process ProcessHandle { get; }

    public IPEndPoint RemoteEndPoint { get; }

    public static implicit operator MatchInfo(RemoteMatchServer matchServer) =>
      new MatchInfo(matchServer.MatchId, matchServer.RemoteEndPoint);

    public override string ToString() =>
      $"<{nameof(RemoteMatchServer)}" +
      $":<{nameof(Host)}:{Host}>" +
      $":<{nameof(MatchId)}:{MatchId}>" +
      $":<{RemoteEndPoint}>>";
  }
}
