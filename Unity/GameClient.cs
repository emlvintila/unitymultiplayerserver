using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using UnityMultiplayer.Shared;
using UnityMultiplayer.Shared.Messaging;

namespace UnityMultiplayer.Unity
{
  /// <summary>
  ///   Talks to a game server or match server on behalf of the player.
  /// </summary>
  public class GameClient : NetworkUser, IMessageHandler<GameClient, Message>
  {
    /// <summary>
    ///   Represents the states that a <see cref="GameClient" /> can be in.
    /// </summary>
    public enum GameClientState
    {
      Disconnected,
      ConnectedToGlobalServer,
      ConnectedToGameServer,
      JoiningMatchServer,
      ConnectedToMatchServer
    }

    public GameClient(MessageReceivedEventHandler<GameClient, Message> handler,
      SimpleEventHandler<GameClient, Exception> exceptionHandler = null) : base(new TcpClient())
    {
      UserEventHandler = handler;
      UserExceptionHandler = exceptionHandler;
    }

    protected MessageReceivedEventHandler<GameClient, Message> UserEventHandler { get; }

    protected SimpleEventHandler<GameClient, Exception> UserExceptionHandler { get; }

    /// <summary>
    ///   The current state of the game client.
    /// </summary>
    public GameClientState State { get; protected set; }

    /// <summary>
    ///   <para>An object that holds information about the current state of the game client.</para>
    ///   <para><see cref="GameClientState.Disconnected" /> -> null</para>
    ///   <para><see cref="GameClientState.ConnectedToGlobalServer" /> -> The <see cref="IPEndPoint" /> of the remote server</para>
    ///   <para><see cref="GameClientState.ConnectedToGameServer" /> -> The <see cref="IPEndPoint" /> of the remote server</para>
    ///   <para><see cref="GameClientState.JoiningMatchServer" /> -> The <see cref="IPEndPoint" /> of the remote server</para>
    ///   <para><see cref="GameClientState.ConnectedToMatchServer" /> -> null</para>
    /// </summary>
    public object StateData { get; protected set; }

    public MessageHandler<GameClient, Message> MessageHandler { get; protected set; }

    public override void Dispose()
    {
      base.Dispose();
      MessageHandler.Dispose();
    }

    protected async void InternalOnMessageReceived(GameClient self, Message message)
    {
      Log.Debug($"Received message {message}");
      switch (message.MessageType)
      {
        case MessageType.ServerRedirect:
          Disconnect();
          IPEndPoint gameServer = MessageDecoder.ServerRedirect(message);
          await ConnectToGameServer(gameServer.Address.ToString());
          break;
        case MessageType.CreateMatchResponse:
        {
          MatchInfo matchInfo = MessageDecoder.CreateMatchResponse(message);
          await JoinMatchServerInternal(matchInfo);
          MatchRoomCreated?.Invoke(matchInfo);
          break;
        }
        case MessageType.JoinMatchResponseAccepted:
        {
          MatchInfo matchInfo = MessageDecoder.JoinMatchResponseAccepted(message);
          await JoinMatchServerInternal(matchInfo);
          MatchRoomJoined?.Invoke(matchInfo);
          break;
        }
        default:
          return;
      }
    }

    /// <inheritdoc cref="NetworkUser.Disconnect" />
    public override void Disconnect(int timeout = Constants.SOCKET_TIMEOUT)
    {
      MessageHandler.StopReadLoop();
      MessageHandler.Dispose();
      base.Disconnect(timeout);
      SetStateDisconnected();
    }

    protected async Task JoinMatchServerInternal(MatchInfo matchInfo)
    {
      Disconnect();
      IPEndPoint ipEndPoint = Utils.IpEndPointFromString(matchInfo.EndPointString);
      SetState(GameClientState.JoiningMatchServer, ipEndPoint);
      await ConnectToMatchServer(ipEndPoint);
    }

    protected void InitMessageHandler()
    {
      MessageHandler = new MessageHandler<GameClient, Message>(this, TcpClient.GetStream(),
        MessageEncoder.ToByteArray, MessageDecoder.FromByteArray);
      MessageHandler.MessageReceived += InternalOnMessageReceived;
      MessageHandler.MessageReceived += UserEventHandler;
      MessageHandler.StreamOperationException += InternalExceptionHandler;
      MessageHandler.StreamOperationException += UserExceptionHandler;
      MessageHandler.StartReadLoop();
    }

    /// <summary>
    ///   Aborts the connection to the remote server.
    /// </summary>
    /// <param name="self"></param>
    /// <param name="exception"></param>
    protected void InternalExceptionHandler(GameClient self, Exception exception)
    {
      Log.Error("", exception);
      Disconnect(0);
    }

    protected void SetStateDisconnected()
    {
      SetState(GameClientState.Disconnected, null);
    }

    /// <summary>
    ///   Invoked after a match room was successfully created.
    /// </summary>
    public event SimpleEventHandler<MatchInfo> MatchRoomCreated;

    /// <summary>
    ///   Invoked after successfully joining a match room.
    /// </summary>
    public event SimpleEventHandler<MatchInfo> MatchRoomJoined;

    protected void SetState(GameClientState state, object data)
    {
      State = state;
      StateData = data;
      Log.Info($"Set state {state}");
    }

    /// <summary>
    ///   Connects to the global server.
    /// </summary>
    /// <param name="host"></param>
    /// <returns></returns>
    public async Task ConnectToGlobal(string host)
    {
      Log.Info($"Connecting to global server {host}");
      await TcpClient.ConnectAsync(host, Constants.GLOBAL_SERVER_PORT).ContinueWith(ConnectToGlobalCallback);
    }

    /// <summary>
    ///   Connects to a game server.
    /// </summary>
    /// <param name="host"></param>
    /// <returns></returns>
    protected async Task ConnectToGameServer(string host)
    {
      Log.Info($"Connecting to game server {host}");
      await TcpClient.ConnectAsync(host, Constants.GAME_SERVER_PORT).ContinueWith(ConnectToGameServerCallback);
    }

    /// <summary>
    ///   Connects to a match server.
    /// </summary>
    /// <param name="ipEndPoint"></param>
    /// <returns></returns>
    protected async Task ConnectToMatchServer(IPEndPoint ipEndPoint)
    {
      Log.Info($"Connecting to match server {ipEndPoint}");
      await TcpClient.ConnectAsync(ipEndPoint.Address, ipEndPoint.Port).ContinueWith(ConnectToMatchServerCallback);
    }

    protected void ConnectToMatchServerCallback(Task connectTask)
    {
      if (connectTask.IsCompletedSuccessfully())
      {
        Log.Info("Successfully connected to the match server");
        InitMessageHandler();
        SetState(GameClientState.ConnectedToMatchServer, null);
      }
      else
      {
        Log.Warn("Failed to connect to the match server", connectTask.Exception);
        SetStateDisconnected();
      }
    }

    protected void ConnectToGameServerCallback(Task connectTask)
    {
      if (connectTask.IsCompletedSuccessfully())
      {
        Log.Info("Successfully connected to the game server");
        InitMessageHandler();
        SetState(GameClientState.ConnectedToGameServer, (IPEndPoint) TcpClient.Client.RemoteEndPoint);
      }
      else
      {
        Log.Warn("Failed to connect to the game server", connectTask.Exception);
        SetStateDisconnected();
      }
    }

    protected void ConnectToGlobalCallback(Task connectTask)
    {
      if (connectTask.IsCompletedSuccessfully())
      {
        Log.Info("Successfully connected to the global server");
        InitMessageHandler();
        SetState(GameClientState.ConnectedToGlobalServer, (IPEndPoint) TcpClient.Client.RemoteEndPoint);
      }
      else
      {
        Log.Warn("Failed to connect to the global server", connectTask.Exception);
        SetStateDisconnected();
      }
    }

    /// <summary>
    ///   Sends a create match message to the game server.
    /// </summary>
    /// <exception cref="InvalidOperationException">Thrown if the client is not currently connected to a game server.</exception>
    public void CreateMatchRoom()
    {
      if (State != GameClientState.ConnectedToGameServer)
        throw new InvalidOperationException();

      Log.Info("Creating match");
      Message message = MessageEncoder.CreateMatch();
      MessageHandler.SendMessage(message);
    }

    /// <summary>
    ///   Sends a join match message to the game server.
    /// </summary>
    /// <param name="matchId">The id of the match to join.</param>
    /// <exception cref="InvalidOperationException">Thrown if the client is not currently connected to a game server.</exception>
    public void JoinMatchRoom(int matchId)
    {
      if (State != GameClientState.ConnectedToGameServer)
        throw new InvalidOperationException();

      Log.Info($"Joining match with id {matchId}");
      Message message = MessageEncoder.JoinMatch(matchId);
      MessageHandler.SendMessage(message);
    }
  }
}
