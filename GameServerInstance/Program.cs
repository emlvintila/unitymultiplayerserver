using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Layout;
using UnityMultiplayer.GameServer;
using UnityMultiplayer.Shared;

namespace UnityMultiplayer.GameServerInstance
{
  internal class Program
  {
    private static GameServer.GameServer gameServer;

    private static async Task Main(string[] args)
    {
      BasicConfigurator.Configure(LogManager.GetRepository(Assembly.GetExecutingAssembly()),
        new ConsoleAppender
        {
          Layout = new PatternLayout("%-5level %date{HH:mm:ss} %-20logger{1}%message%newline%exception")
        });
      gameServer = new GameServer.GameServer(args[0]) {ListenPort = Constants.GAME_SERVER_PORT};
      await gameServer.StartAsync();
      gameServer.GameServerClient.MessageHandler.StreamOperationException += GameServerOnStreamOperationException;
      while (true) { }

      // ReSharper disable once FunctionNeverReturns
    }

#pragma warning disable 1998
    private static async void GameServerOnStreamOperationException(GameServerClient client, Exception ex)
#pragma warning restore 1998
    {
      if (ex is IOException ioException)
      {
        // The GameServer was disconnected from the MasterServer
        // await GameServer.GameServerClient.ConnectToMaster();
        // TODO: Log the exception
        Console.WriteLine(ioException);
        client.MessageHandler.StopReadLoop();
      }
    }
  }
}
