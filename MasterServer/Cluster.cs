using System.Linq;
using System.Threading.Tasks;
using log4net;
using UnityMultiplayer.Shared;
using UnityMultiplayer.Shared.Messaging;

namespace UnityMultiplayer.MasterServer
{
  /// <summary>
  ///   Represents a <see cref="UnityMultiplayer.MasterServer.MasterServer" /> and a global server.
  ///   Players connect to the global server which acts as a load balancer.
  /// </summary>
  public class Cluster
  {
    public Cluster()
    {
      MasterServer = new MasterServer {ListenPort = Constants.MASTER_SERVER_PORT};
      GlobalServer = new Server {ListenPort = Constants.GLOBAL_SERVER_PORT};
      GlobalServer.ClientConnected += GlobalServerOnClientConnected;
      Log = LogManager.GetLogger(GetType());
    }

    protected ILog Log { get; }

    public MasterServer MasterServer { get; }
    public Server GlobalServer { get; }

    /// <summary>
    ///   Redirects the connected player to an appropriate game server.
    /// </summary>
    /// <param name="user"></param>
    protected void GlobalServerOnClientConnected(AuthenticatedUser user)
    {
      RemoteGameServerClient remoteGameServer = MasterServer.GameServers.First();
      Log.Info($"Redirecting {user} to {remoteGameServer}");
      user.MessageHandler.SendMessage(MessageEncoder.ServerRedirect(remoteGameServer.RemoteEndPoint));
      GlobalServer.DisconnectUser(user, true);
    }

    /// <summary>
    ///   Starts both the master server and the global server.
    /// </summary>
    /// <returns></returns>
    public async Task StartAsync()
    {
      await MasterServer.StartAsync();
      await GlobalServer.StartAsync();
    }
  }
}
