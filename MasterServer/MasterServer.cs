using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityMultiplayer.Shared;
using UnityMultiplayer.Shared.Messaging;

namespace UnityMultiplayer.MasterServer
{
  /// <summary>
  ///   A server that other servers connect to.
  /// </summary>
  public class MasterServer : AbstractServer
  {
    public MasterServer() => base.ClientConnected += BaseOnClientConnected;

    protected List<RemoteGameServerClient> GameServerList { get; } = new List<RemoteGameServerClient>();

    /// <summary>
    ///   The available game servers.
    /// </summary>
    public IEnumerable<RemoteGameServerClient> GameServers => GameServerList;

    public new event SimpleEventHandler<RemoteGameServerClient> ClientConnected;

    /// <summary>
    ///   Authenticates the remote game server and starts the message loop.
    /// </summary>
    /// <param name="client"></param>
    protected void BaseOnClientConnected(TcpClient client)
    {
      // TODO: Authenticate remote server
      RemoteGameServerClient remoteGameServerClient = new RemoteGameServerClient(client);
      remoteGameServerClient.MessageHandler.MessageReceived += RemoteGameServerOnMessageReceived;
      remoteGameServerClient.MessageHandler.StartReadLoop();
      GameServerList.Add(remoteGameServerClient);
    }

    /// <summary>
    ///   Handles received messages from servers.
    /// </summary>
    /// <param name="server"></param>
    /// <param name="message"></param>
    protected void RemoteGameServerOnMessageReceived(RemoteGameServerClient server, ServerMessage message)
    {
      switch (message.MessageType)
      {
        case ServerMessageType.AnnounceGameServerReady:
          string ipString = Encoding.Unicode.GetString(message.Data);
          IPAddress ipAddress = IPAddress.Parse(ipString);
          server.RemoteEndPoint = new IPEndPoint(ipAddress, Constants.GAME_SERVER_PORT);
          Log.Debug($"Received {message} from {server}");
          // Invoke the ClientConnected event only after the server announced itself
          ClientConnected?.Invoke(server);
          break;
        default:
          return;
      }
    }
  }
}
