﻿using System.Net;
using System.Net.Sockets;
using UnityMultiplayer.Shared;
using UnityMultiplayer.Shared.Messaging;

namespace UnityMultiplayer.MasterServer
{
  /// <summary>
  ///   Represents a remote game server.
  /// </summary>
  public class RemoteGameServerClient : NetworkUser, IMessageHandler<RemoteGameServerClient, ServerMessage>
  {
    public RemoteGameServerClient(TcpClient tcpClient) : base(tcpClient) =>
      MessageHandler = new MessageHandler<RemoteGameServerClient, ServerMessage>(this, TcpClient.GetStream(),
        ServerMessageEncoder.ToByteArray, ServerMessageDecoder.FromByteArray);

    public IPEndPoint RemoteEndPoint { get; set; }
    public MessageHandler<RemoteGameServerClient, ServerMessage> MessageHandler { get; }

    public override void Dispose()
    {
      base.Dispose();
      MessageHandler.Dispose();
    }

    public override string ToString() =>
      $"<{nameof(RemoteGameServerClient)}" +
      $"<{nameof(RemoteEndPoint)}:{RemoteEndPoint}>>";
  }
}
