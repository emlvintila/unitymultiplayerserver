using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Layout;
using UnityMultiplayer.MasterServer;

namespace UnityMultiplayer.ClusterInstance
{
  internal class Program
  {
    private static async Task Main(string[] args)
    {
      BasicConfigurator.Configure(LogManager.GetRepository(Assembly.GetExecutingAssembly()),
        new ConsoleAppender
        {
          Layout = new PatternLayout("%-5level %date{HH:mm:ss} %-20logger{1}%message%newline%exception")
        });
      Cluster cluster = new Cluster();
      await cluster.StartAsync();
      cluster.MasterServer.ClientConnected += MasterServerOnClientConnected;
      while (true) { }

      // ReSharper disable once FunctionNeverReturns
    }

    private static void MasterServerOnClientConnected(RemoteGameServerClient gameServer)
    {
      gameServer.MessageHandler.StreamOperationException += GameServerOnStreamOperationException;
    }

    private static void GameServerOnStreamOperationException(RemoteGameServerClient gameServer, Exception ex)
    {
      // TODO: Log the exception
      Debug.WriteLine(ex);
      gameServer.MessageHandler.StopReadLoop();
    }
  }
}
