namespace UnityMultiplayer.Shared.Messaging
{
  /// <summary>
  ///   Specifies that a class can handle sending and receiving messages.
  /// </summary>
  /// <typeparam name="TUser">The type of user to handle messages from.</typeparam>
  /// <typeparam name="TMessage">The type of message to handle.</typeparam>
  public interface IMessageHandler<TUser, TMessage>
    where TUser : NetworkInfrastructureEntity
    where TMessage : AbstractMessage
  {
    MessageHandler<TUser, TMessage> MessageHandler { get; }
  }
}
