using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace UnityMultiplayer.Shared.Messaging
{
  /// <summary>
  ///   Handles sending and receiving messages.
  /// </summary>
  /// <typeparam name="TUser">The type of user to handle messages from.</typeparam>
  /// <typeparam name="TMessage">The type of message to handle.</typeparam>
  public class MessageHandler<TUser, TMessage> : IDisposable
    where TUser : NetworkInfrastructureEntity
    where TMessage : AbstractMessage
  {
    public delegate TMessage FromByteArrayDelegate(byte[] bytes);

    public delegate byte[] ToByteArrayDelegate(TMessage message);

    public MessageHandler(TUser owner, Stream stream, ToByteArrayDelegate toByteArrayDelegate,
      FromByteArrayDelegate fromByteArrayDelegate)
    {
      Owner = owner;
      Stream = stream;
      ToByteArray = toByteArrayDelegate;
      FromByteArray = fromByteArrayDelegate;
      TokenSource = new CancellationTokenSource();
    }

    protected bool Disposed { get; set; }

    protected byte[] Buffer { get; } = new byte[Constants.MESSAGE_BUFFER_SIZE];

    public bool ReadLoopActive { get; protected set; }

    public TUser Owner { get; }

    protected ToByteArrayDelegate ToByteArray { get; }

    protected FromByteArrayDelegate FromByteArray { get; }

    protected CancellationTokenSource TokenSource { get; }

    protected Stream Stream { get; }

    protected MessageReceivedEventHandler<TUser, TMessage> MessageReceivedHandlers { get; set; }

    protected SimpleEventHandler<TUser, Exception> StreamOperationExceptionHandlers { get; set; }

    public void Dispose()
    {
      if (Disposed)
        return;

      Disposed = true;
      DoStopReadLoop();
      TokenSource?.Dispose();
      MessageReceivedHandlers = null;
      StreamOperationExceptionHandlers = null;
    }

    public event MessageReceivedEventHandler<TUser, TMessage> MessageReceived
    {
      add
      {
        ThrowIfDisposed();
        MessageReceivedHandlers += value;
      }
      remove
      {
        ThrowIfDisposed();
        // ReSharper disable once DelegateSubtraction
        MessageReceivedHandlers -= value;
      }
    }

    public event SimpleEventHandler<TUser, Exception> StreamOperationException
    {
      add
      {
        ThrowIfDisposed();
        StreamOperationExceptionHandlers += value;
      }
      remove
      {
        ThrowIfDisposed();
        // ReSharper disable once DelegateSubtraction
        StreamOperationExceptionHandlers -= value;
      }
    }

    public void StartReadLoop()
    {
      ThrowIfDisposed();
      if (ReadLoopActive)
        return;

      DoReadLoop();
    }

    public void StopReadLoop()
    {
      ThrowIfDisposed();
      DoStopReadLoop();
    }

    protected void DoStopReadLoop()
    {
      ReadLoopActive = false;
      TokenSource?.Cancel();
    }

    protected async void DoReadLoop()
    {
      if (Disposed)
        return;

      ReadLoopActive = true;
      while (ReadLoopActive)
      {
        try
        {
          int byteCount = await Stream.ReadAsync(Buffer, 0, Buffer.Length, TokenSource.Token);
          if (byteCount <= 0)
            continue;

          byte[] bytes = new byte[byteCount];
          System.Buffer.BlockCopy(Buffer, 0, bytes, 0, byteCount);
          TMessage message = FromByteArray(bytes);
          MessageReceivedHandlers?.Invoke(Owner, message);
        }
        catch (Exception ex)
          when (Disposed &&
            (ex is ObjectDisposedException || ex.InnerException is ObjectDisposedException)) { }
        catch (Exception ex) when (ex is IOException)
        {
          StreamOperationExceptionHandlers?.Invoke(Owner, ex);
        }
      }
    }

    /// <summary>
    ///   Writes a message to the <see cref="Stream" />.
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    public async Task SendMessage(TMessage message)
    {
      ThrowIfDisposed();
      byte[] buffer = ToByteArray(message);
      try
      {
        await Stream.WriteAsync(buffer, 0, buffer.Length, TokenSource.Token);
      }
      catch (Exception ex)
        when (Disposed && (ex is ObjectDisposedException || ex.InnerException is ObjectDisposedException)) { }
      catch (Exception ex) when (ex is IOException)
      {
        StreamOperationExceptionHandlers?.Invoke(Owner, ex);
      }
    }

    protected void ThrowIfDisposed()
    {
      if (Disposed)
        throw new ObjectDisposedException(GetType().FullName);
    }
  }
}
