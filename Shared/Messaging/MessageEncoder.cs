﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace UnityMultiplayer.Shared.Messaging
{
  /// <summary>
  ///   Transforms objects into <see cref="Message" />s.
  /// </summary>
  public static class MessageEncoder
  {
    internal static BinaryFormatter BinaryFormatter { get; } = new BinaryFormatter();

    internal static Message WithEmptyData(MessageType messageType) => new Message(messageType, new byte[0]);

    internal static Message Invalid() => WithEmptyData(MessageType.Invalid);

    public static Message ServerRedirect(IPEndPoint remoteEndPoint) =>
      new Message(MessageType.ServerRedirect, Encoding.Unicode.GetBytes(remoteEndPoint.ToString()));

    public static Message CreateMatch() => WithEmptyData(MessageType.CreateMatch);

    public static Message CreateMatchResponse(MatchInfo matchInfo) =>
      new Message(MessageType.CreateMatchResponse, Utils.SerializeObject(matchInfo));

    public static Message JoinMatch(int matchId) => new Message(MessageType.JoinMatch, Utils.IntToByteArray(matchId));

    public static Message JoinMatchResponseAccepted(MatchInfo matchInfo) =>
      new Message(MessageType.JoinMatchResponseAccepted, Utils.SerializeObject(matchInfo));

    public static Message JoinMatchResponseDeclined() => WithEmptyData(MessageType.JoinMatchResponseDeclined);

    public static Message GetMatchList() => WithEmptyData(MessageType.GetMatchList);

    public static Message PopulateMatchList(IEnumerable<MatchInfo> matches) =>
      new Message(MessageType.PopulateMatchList, Utils.SerializeObject(matches));

    public static Message ClientGameUpdate(byte[] data) => new Message(MessageType.ClientGameUpdate, data);

    public static Message ServerGameUpdate(byte[] data) => new Message(MessageType.ServerGameUpdate, data);

    public static Message InitNetworkManager(int playerId) =>
      new Message(MessageType.InitNetworkManager, Utils.IntToByteArray(playerId));

    public static Message LoadScene(string sceneName) =>
      new Message(MessageType.LoadScene, Encoding.Unicode.GetBytes(sceneName));

    public static Message Custom(byte[] data) => new Message(MessageType.Custom, data);

    public static byte[] ToByteArray(Message message)
    {
      byte[] buffer = new byte[message.Data.Length + 1];
      Buffer.SetByte(buffer, 0, (byte) message.MessageType);
      Buffer.BlockCopy(message.Data, 0, buffer, 1, message.Data.Length);
      return buffer;
    }
  }
}
