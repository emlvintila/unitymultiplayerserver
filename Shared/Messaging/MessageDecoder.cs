using System;
using System.Collections.Generic;
using System.Net;

namespace UnityMultiplayer.Shared.Messaging
{
  /// <summary>
  ///   Transforms <see cref="Message" />s into actual objects.
  /// </summary>
  public static class MessageDecoder
  {
    public static IPEndPoint ServerRedirect(Message message)
    {
      if (message.MessageType != MessageType.ServerRedirect)
        throw new ArgumentException(nameof(message));

      return Utils.IpEndPointFromString(message.Data.AsString());
    }

    public static MatchInfo CreateMatchResponse(Message message)
    {
      if (message.MessageType != MessageType.CreateMatchResponse)
        throw new ArgumentException(nameof(message));

      return Utils.DeserializeObject<MatchInfo>(message.Data);
    }

    public static int JoinMatch(Message message)
    {
      if (message.MessageType != MessageType.JoinMatch)
        throw new ArgumentException(nameof(message));

      return Utils.ByteArrayToInt(message.Data);
    }

    public static MatchInfo JoinMatchResponseAccepted(Message message)
    {
      if (message.MessageType != MessageType.JoinMatchResponseAccepted)
        throw new ArgumentException(nameof(message));

      return Utils.DeserializeObject<MatchInfo>(message.Data);
    }

    public static IEnumerable<MatchInfo> PopulateMatchList(Message message)
    {
      if (message.MessageType != MessageType.PopulateMatchList)
        throw new ArgumentException(nameof(message));

      return Utils.DeserializeObject<IEnumerable<MatchInfo>>(message.Data);
    }

    public static int InitNetworkManager(Message message)
    {
      if (message.MessageType != MessageType.InitNetworkManager)
        throw new ArgumentException(nameof(message));

      return Utils.ByteArrayToInt(message.Data);
    }

    public static string LoadScene(Message message)
    {
      if (message.MessageType != MessageType.LoadScene)
        throw new ArgumentException(nameof(message));

      return message.Data.AsString();
    }

    public static Message FromByteArray(byte[] bytes)
    {
      byte[] data = new byte[bytes.Length - 1];
      MessageType messageType = (MessageType) Buffer.GetByte(bytes, 0);
      Buffer.BlockCopy(bytes, 1, data, 0, data.Length);
      return new Message(messageType, data);
    }
  }
}
