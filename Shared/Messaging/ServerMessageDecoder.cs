using System;
using System.Net;

namespace UnityMultiplayer.Shared.Messaging
{
  /// <summary>
  ///   Transforms <see cref="ServerMessage" />s into actual objects.
  /// </summary>
  public static class ServerMessageDecoder
  {
    public static IPAddress AnnounceGameServerReady(ServerMessage message)
    {
      if (message.MessageType != ServerMessageType.AnnounceGameServerReady)
        throw new ArgumentException(nameof(message));

      return IPAddress.Parse(message.Data.AsString());
    }

    public static IPEndPoint AnnounceMatchServerReady(ServerMessage message)
    {
      if (message.MessageType != ServerMessageType.AnnounceMatchServerReady)
        throw new ArgumentException(nameof(message));

      return Utils.IpEndPointFromString(message.Data.AsString());
    }

    public static ServerMessage FromByteArray(byte[] bytes)
    {
      byte[] data = new byte[bytes.Length - 1];
      ServerMessageType messageType = (ServerMessageType) Buffer.GetByte(bytes, 0);
      Buffer.BlockCopy(bytes, 1, data, 0, data.Length);
      return new ServerMessage(messageType, data);
    }
  }
}
