using System;

namespace UnityMultiplayer.Shared.Messaging
{
  /// <summary>
  ///   Represents a message sent between servers.
  /// </summary>
  [Serializable]
  public class ServerMessage : AbstractMessage
  {
    protected ServerMessage() { }

    internal ServerMessage(ServerMessageType messageType, byte[] data) : base(data) => MessageType = messageType;

    public ServerMessageType MessageType { get; }

    public override string ToString() =>
      $"<{nameof(ServerMessage)}:{Enum.GetName(typeof(ServerMessageType), MessageType)}>";
  }

  public enum ServerMessageType : byte
  {
    Invalid = 0,
    AnnounceGameServerReady = 1,
    MasterServerPing = 2,
    GameServerPong = 3,

    AnnounceMatchServerReady = 254
  }
}
