using System;
using System.Net;
using System.Text;

namespace UnityMultiplayer.Shared.Messaging
{
  /// <summary>
  ///   Transforms objects into <see cref="ServerMessage" />s.
  /// </summary>
  public static class ServerMessageEncoder
  {
    internal static ServerMessage Invalid => WithEmptyData(ServerMessageType.Invalid);

    internal static ServerMessage WithEmptyData(ServerMessageType messageType) =>
      new ServerMessage(messageType, new byte[0]);

    public static ServerMessage AnnounceGameServerReady(IPAddress ipAddress) =>
      new ServerMessage(ServerMessageType.AnnounceGameServerReady,
        Encoding.Unicode.GetBytes(ipAddress.ToString()));

    public static ServerMessage MasterServerPing() => WithEmptyData(ServerMessageType.MasterServerPing);

    public static ServerMessage GameServerPong() => WithEmptyData(ServerMessageType.GameServerPong);

    public static ServerMessage AnnounceMatchServerReady(IPEndPoint ipEndPoint) =>
      new ServerMessage(ServerMessageType.AnnounceMatchServerReady,
        Encoding.Unicode.GetBytes(ipEndPoint.ToString()));

    public static byte[] ToByteArray(ServerMessage message)
    {
      byte[] buffer = new byte[message.Data.Length + 1];
      Buffer.SetByte(buffer, 0, (byte) message.MessageType);
      Buffer.BlockCopy(message.Data, 0, buffer, 1, message.Data.Length);
      return buffer;
    }
  }
}
