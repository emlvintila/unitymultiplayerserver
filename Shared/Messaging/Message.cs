﻿using System;

namespace UnityMultiplayer.Shared.Messaging
{
  /// <summary>
  ///   Represents a message sent between servers and clients.
  /// </summary>
  [Serializable]
  public class Message : AbstractMessage
  {
    protected Message() { }

    internal Message(MessageType messageType, byte[] data) : base(data) => MessageType = messageType;

    /// <summary>
    ///   The type of the message.
    /// </summary>
    public MessageType MessageType { get; }

    public override string ToString() => $"<{nameof(Message)}:{Enum.GetName(typeof(MessageType), MessageType)}>";
  }

  public enum MessageType : byte
  {
    Invalid = 0,
    ServerRedirect = 1,
    CreateMatch = 2,
    CreateMatchResponse = 3,
    JoinMatch = 4,
    JoinMatchResponseAccepted = 5,
    JoinMatchResponseDeclined = 6,
    GetMatchList = 7,
    PopulateMatchList = 8,

    ClientGameUpdate = 251,
    ServerGameUpdate = 252,
    InitNetworkManager = 253,
    LoadScene = 254,
    Custom = 255
  }
}
