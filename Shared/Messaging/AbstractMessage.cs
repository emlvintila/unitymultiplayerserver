using System;

namespace UnityMultiplayer.Shared.Messaging
{
  /// <summary>
  ///   Represents a message as an array of bytes.
  /// </summary>
  [Serializable]
  public abstract class AbstractMessage
  {
    protected AbstractMessage() { }

    internal AbstractMessage(byte[] data) => Data = data;

    /// <summary>
    ///   The data of the message to be interpreted by the receiving party.
    /// </summary>
    public byte[] Data { get; }
  }
}
