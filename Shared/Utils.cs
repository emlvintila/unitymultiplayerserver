using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace UnityMultiplayer.Shared
{
  public static class Utils
  {
    internal static BinaryFormatter BinaryFormatter { get; } = new BinaryFormatter();

    public static byte[] IntToByteArray(int i)
    {
      using MemoryStream memoryStream = new MemoryStream();
      using BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
      binaryWriter.Write(i);
      return memoryStream.ToArray();
    }

    public static int ByteArrayToInt(byte[] bytes)
    {
      if (bytes.Length != sizeof(int))
        throw new ArgumentException(nameof(bytes));

      unsafe
      {
        fixed (byte* ptr = bytes)
        {
          return *(int*) ptr;
        }
      }
    }

    public static IPEndPoint IpEndPointFromString(string ipEndPointString)
    {
      int index = ipEndPointString.LastIndexOf(':');
      string ipAddressString = ipEndPointString.Substring(0, index);
      string portString = ipEndPointString.Substring(index + 1);
      return new IPEndPoint(IPAddress.Parse(ipAddressString), int.Parse(portString));
    }

    public static byte[] SerializeObject<T>(T obj)
    {
      using MemoryStream memoryStream = new MemoryStream();
      BinaryFormatter.Serialize(memoryStream, obj);
      return memoryStream.ToArray();
    }

    public static T DeserializeObject<T>(byte[] bytes)
    {
      using MemoryStream memoryStream = new MemoryStream(bytes);
      return (T) BinaryFormatter.Deserialize(memoryStream);
    }

    /// <summary>
    ///   Formats an argument in the long unix-style format (--name=value).
    /// </summary>
    /// <param name="name">The name of the argument that comes before the equals sign.</param>
    /// <param name="value">The value of the argument that comes after the equals sign.</param>
    /// <returns></returns>
    public static string FormatArgument(string name, object value) => $"--{name}={value}";

    /// <summary>
    ///   Returns a task that completes when the specified port is no longer available.
    /// </summary>
    /// <param name="port"></param>
    /// <param name="millisecondsBetweenChecks"></param>
    /// <returns></returns>
    public static async Task WaitForPortListen(int port, int millisecondsBetweenChecks = 1000)
    {
      while (true)
      {
        bool flag = IPGlobalProperties
          .GetIPGlobalProperties()
          .GetActiveTcpListeners()
          .FirstOrDefault(ipEndPoint => ipEndPoint.Port == port) != null;
        if (flag)
          break;

        await Task.Delay(millisecondsBetweenChecks);
      }
    }
  }
}
