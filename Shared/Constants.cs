namespace UnityMultiplayer.Shared
{
  public struct Constants
  {
    public const int MESSAGE_BUFFER_SIZE = 4 * 1024 + 1;
    public const string MASTER_SERVER_IP_ADDRESS = "127.0.0.1";
    public const int MASTER_SERVER_PORT = 21713;
    public const int GLOBAL_SERVER_PORT = 21714;
    public const int GAME_SERVER_PORT = 21715;

    public const string PORT_ARG_NAME = "port";
    public const int SOCKET_TIMEOUT = 5;
    public const int SOCKET_TIMEOUT_MAX = -1;
  }
}
