using UnityMultiplayer.Shared.Messaging;

namespace UnityMultiplayer.Shared
{
  public delegate void SimpleEventHandler();

  public delegate void SimpleEventHandler<in T>(T t);

  public delegate void SimpleEventHandler<in TSender, in TArgs>(TSender sender, TArgs args);

  public delegate void MessageReceivedEventHandler<in TUser, in TMessage>(TUser user, TMessage message)
    where TUser : NetworkInfrastructureEntity
    where TMessage : AbstractMessage;
}
