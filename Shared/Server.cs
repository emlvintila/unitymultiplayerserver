using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace UnityMultiplayer.Shared
{
  /// <summary>
  ///   Base implementation of the <see cref="AbstractServer" />.
  /// </summary>
  public class Server : AbstractServer
  {
    public Server() => base.ClientConnected += BaseOnClientConnected;

    /// <summary>
    ///   Maps ids to <see cref="AuthenticatedUser" /> instances.
    /// </summary>
    public IReadOnlyDictionary<int, AuthenticatedUser> AuthenticatedUsers => AuthenticatedUsersDict;

    protected Dictionary<int, AuthenticatedUser> AuthenticatedUsersDict { get; } =
      new Dictionary<int, AuthenticatedUser>();

    public new event SimpleEventHandler<AuthenticatedUser> ClientConnected;

    protected virtual void BaseOnClientConnected(TcpClient client)
    {
      // TODO: Authenticate user
      AuthenticatedUser user = new AuthenticatedUser(client, new Random().Next());
      Log.Info($"Client {client.Client.RemoteEndPoint} authenticated as {user} using password authentication");
      AuthenticatedUsersDict[user.Id] = user;
      InvokeClientConnected(user);
    }

    protected void InvokeClientConnected(AuthenticatedUser user)
    {
      ClientConnected?.Invoke(user);
    }

    /// <summary>
    ///   Disconnects the specified user from the server.
    /// </summary>
    /// <param name="user">The user to be disconnected.</param>
    /// <param name="graceful">Whether to do a graceful disconnect or forcefully abort the connection.</param>
    public void DisconnectUser(AuthenticatedUser user, bool graceful)
    {
      Log.Info($"Disconnecting user {user}");
      AuthenticatedUsersDict.Remove(user.Id);
      user.Disconnect(graceful ? Constants.SOCKET_TIMEOUT_MAX : 0);
      user.Dispose();
    }
  }
}
