using log4net;

namespace UnityMultiplayer.Shared
{
  /// <summary>
  ///   Base class for any entity that uses the network infrastructure.
  /// </summary>
  public abstract class NetworkInfrastructureEntity
  {
    protected NetworkInfrastructureEntity() => Log = LogManager.GetLogger(GetType());

    protected ILog Log { get; }
  }
}
