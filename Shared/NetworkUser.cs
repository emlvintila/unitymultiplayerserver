﻿using System;
using System.Net.Sockets;

namespace UnityMultiplayer.Shared
{
  /// <summary>
  ///   Base class for a network entity that uses a <see cref="System.Net.Sockets.TcpClient" />.
  /// </summary>
  public class NetworkUser : NetworkInfrastructureEntity, IDisposable
  {
    protected TcpClient _client;

    protected NetworkUser(TcpClient tcpClient) => _client = tcpClient;

    public bool IsConnected => (TcpClient?.Connected).GetValueOrDefault();

    protected TcpClient TcpClient
    {
      get
      {
        if (_client is null)
          return _client = InitTcpClient();

        try
        {
          _ = _client.Available;
          return _client;
        }
        catch (ObjectDisposedException)
        {
          return _client = InitTcpClient();
        }
      }
    }

    public virtual void Dispose()
    {
      _client?.Dispose();
    }

    protected virtual TcpClient InitTcpClient() => new TcpClient();

    /// <summary>
    ///   Gracefully disconnects from the remote server.
    /// </summary>
    /// <param name="timeout">The maximum time in seconds to wait before aborting the connection.</param>
    public virtual void Disconnect(int timeout = Constants.SOCKET_TIMEOUT)
    {
      if (!IsConnected)
        return;

      DoDisconnect(timeout);
    }

    protected virtual void DoDisconnect(int timeout)
    {
      _client?.Client.Close(timeout);
      _client?.Close();
    }
  }
}
