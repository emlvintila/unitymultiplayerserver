using System.Net.Sockets;
using UnityMultiplayer.Shared.Messaging;

namespace UnityMultiplayer.Shared
{
  /// <summary>
  ///   Represents a user that has been authenticated and is a valid player.
  /// </summary>
  public class AuthenticatedUser : NetworkUser
  {
    public AuthenticatedUser(TcpClient tcpClient, int id) : base(tcpClient)
    {
      Id = id;
      MessageHandler = new MessageHandler<AuthenticatedUser, Message>(this, TcpClient.GetStream(),
        MessageEncoder.ToByteArray, MessageDecoder.FromByteArray);
    }

    public MessageHandler<AuthenticatedUser, Message> MessageHandler { get; }

    public int Id { get; }

    public override void Dispose()
    {
      base.Dispose();
      MessageHandler.Dispose();
    }

    public override string ToString() => $"<{nameof(AuthenticatedUser)}:{Id}>";
  }
}
