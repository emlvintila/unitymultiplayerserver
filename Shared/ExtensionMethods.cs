using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnityMultiplayer.Shared
{
  public static class ByteArrayExtensionMethods
  {
    public static string AsString(this IEnumerable<byte> bytes) => AsString(bytes, Encoding.Unicode);

    public static string AsString(this IEnumerable<byte> bytes, Encoding encoding) =>
      encoding.GetString(bytes.ToArray());
  }

  public static class TaskExtensionMethods
  {
    public static bool IsCompletedSuccessfully(this Task task) => task.Status == TaskStatus.RanToCompletion;
  }
}
