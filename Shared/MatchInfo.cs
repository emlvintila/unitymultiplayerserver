using System;
using System.Net;

namespace UnityMultiplayer.Shared
{
  /// <summary>
  ///   Holds minimal information about a match server.
  /// </summary>
  [Serializable]
  public class MatchInfo
  {
    public MatchInfo(int matchId, IPEndPoint remoteEndPoint)
    {
      MatchId = matchId;
      EndPointString = remoteEndPoint.ToString();
    }

    /// <summary>
    ///   The id of the match.
    /// </summary>
    public int MatchId { get; }

    /// <summary>
    ///   The remote endpoint of the match server.
    /// </summary>
    public string EndPointString { get; }

    public override string ToString() =>
      $"<{nameof(MatchInfo)}" +
      $":<{nameof(MatchId)}:{MatchId}>" +
      $":<{EndPointString}>>";
  }
}
