﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using log4net;

namespace UnityMultiplayer.Shared
{
  /// <summary>
  ///   Represents a server that listens for connections on a specified endpoint.
  /// </summary>
  public abstract class AbstractServer
  {
    protected AbstractServer() => Log = LogManager.GetLogger(GetType());

    protected ILog Log { get; }

    /// <summary>
    ///   The <see cref="IPAddress" /> that the server listens on.
    /// </summary>
    public IPAddress ListenAddress { get; set; } = IPAddress.Any;

    /// <summary>
    ///   The port that the server listens on.
    /// </summary>
    public int ListenPort { get; set; }

    /// <summary>
    ///   The maximum number of pending connections.
    /// </summary>
    public int Backlog { get; } = 10;

    protected TcpListener Listener { get; set; }

    /// <summary>
    ///   Specifies whether to accept new connections.
    /// </summary>
    public bool AcceptClients { get; set; } = true;

    /// <summary>
    ///   Invoked when a client connects to the server.
    /// </summary>
    public event SimpleEventHandler<TcpClient> ClientConnected;

    protected virtual void EnsureValidListenPort()
    {
      if (ListenPort < IPEndPoint.MinPort || ListenPort > IPEndPoint.MaxPort)
        throw new Exception($"Invalid listen port: {ListenPort}");
    }

#pragma warning disable 1998
    /// <summary>
    ///   Starts listening for connections.
    /// </summary>
    /// <returns></returns>
    public virtual async Task StartAsync()
#pragma warning restore 1998
    {
      EnsureValidListenPort();
      Listener = new TcpListener(ListenAddress, ListenPort);
      Listener.Start(Backlog);
      ListenPort = ((IPEndPoint) Listener.LocalEndpoint).Port;
      Log.Info($"Started listening on {Listener.LocalEndpoint}");
      _ = LoopAcceptTcpClient();
    }

    protected async Task LoopAcceptTcpClient()
    {
      while (AcceptClients)
      {
        TcpClient client = await Listener.AcceptTcpClientAsync();
        Log.Info($"Client connected from {client.Client.RemoteEndPoint}");
        ClientConnected?.Invoke(client);
      }
    }

    /// <summary>
    ///   Returns the public ip address of the current machine using the 'dig' command-line utility.
    /// </summary>
    /// <returns></returns>
    protected virtual IPAddress GetOwnPublicAddress()
    {
      return IPAddress.Parse("127.0.0.1");

      Process process =
        Process.Start(new ProcessStartInfo("dig", @"@resolver1.opendns.com A myip.opendns.com +short -4")
        {
          UseShellExecute = false,
          CreateNoWindow = true,
          RedirectStandardOutput = true
        });
      // ReSharper disable once PossibleNullReferenceException
      process.WaitForExit((int) TimeSpan.FromSeconds(15).TotalMilliseconds);
      string ipString = process.StandardOutput.ReadToEnd().Trim();
      return IPAddress.Parse(ipString);
    }

    /// <summary>
    ///   Returns a port that is available.
    /// </summary>
    /// <returns></returns>
    protected static int GetNextAvailablePort()
    {
      TcpListener listener = new TcpListener(IPAddress.Any, 0);
      listener.Start();
      int port = ((IPEndPoint) listener.LocalEndpoint).Port;
      listener.Stop();
      return port;
    }
  }
}
